class MonthlyMembersController < ApplicationController
  before_action :authenticate_user!

  def create
    charge = params[:monthly_member][:charge]
    @monthly_member = MonthlyMember.new(monthly_member_params)
    @monthly_member.uuid = SecureRandom.uuid
    customer = Customer.create(
        charge: charge,
        check_in_time: Time.zone.now,
        rate_per_min: current_user.store.rate_per_min,
        store: current_user.store.id,
        people: 1,
        discount_percentage: 0,
        kind: CustomerKind.find_by_kind('Monthly Member').id,
        monthly_member_id: @monthly_member.id
    )
    @monthly_member.customers << customer
    @monthly_member.save!
    if Rails.env == 'production'
      redirect_to customers_make_payment_request_path(id: customer.id, charge: charge)
    else
      redirect_to customers_path(id: customer.id, charge: charge)
    end
  end

  private

  def monthly_member_params
    params.require(:monthly_member).permit(%i(
    id
    start_date
    end_date
    charge
  ))
  end
end
