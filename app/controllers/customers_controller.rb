class CustomersController < ApplicationController
  before_action :authenticate_user!
  before_action :customers_list_today, only: [:index, :create]

  def index
    @customer = Customer.new
    @num_of_checked_out = Customer.nums_of_checked_out
    @num_of_checked_in = Customer.nums_of_checked_in
  end

  def new
  end

  def show
  end

  # Check-In
  def create
    monthly_member_id = params[:customer][:monthly_member_id]
    if params[:customer][:kind].to_i == CustomerKind.find_by_kind('Monthly Member').id &&
        MonthlyMember.valid_monthly_member(monthly_member_id).empty?
      flash[:alert] = "id: #{monthly_member_id} isn't monthly member"
      redirect_back fallback_location: :index
      return
    else
      @customer = Customer.new(customer_params)
      @customer.update_attributes(
          check_in_time: Time.at(Time.zone.now.to_i / 60 * 60),
          store: current_user.store.id,
          rate_per_min: current_user.store.rate_per_min,
          monthly_member_id: monthly_member_id.empty? ? nil : monthly_member_id.to_i
      )
      @customer.build_payment
    end
    redirect_to customers_path
  end

  def search
    id = params[:search]
    if id.empty?
      redirect_to customers_path
    else
      @customer = Customer.new
      @customers = Customer.where(id: id).page(params[:page])
      render :index
    end
  end

  def search_date
    if params[:datepicker].nil? || params[:datepicker] == ''
      @date = Time.new.strftime("%Y-%m-%d")
    else
      @date = Time.parse(params[:datepicker]) rescue Time.zone.now
    end
    if Time.new.strftime("%Y-%m-%d").eql?(@date)
      redirect_to customers_path
    else
      @customer = Customer.new
      customers_list(@date)
      render :index
    end
  end

  #Check-Out

  def checkout
  end

  def date_change
    d = Time.parse(params[:date])
    @customers = Customer.where(check_in_time: d.beginning_of_day, check_out_time: d.end_of_day, store: current_user.store)
                     .page(params[:page])
                     .reverse_order
    redirect_to customers_path
  end

  def update
    @customer = Customer.find(params[:customer][:id])
    if @customer.update_attributes(customer_params)
      charge = params[:customer][:charge].gsub(/[^0-9]/, '').to_i
      @customer.charge = charge
      if PaymentMethod.find_by_payment_method('DEV_USE_ONLY').id == params[:customer][:payment_method].to_i
        redirect_to customers_path
      elsif (1..5) === params[:customer][:payment_method].to_i
        make_payment_request(@customer.id, charge)
      end
    else
      render :index
    end
  end

  def cancel
    @customer = Customer.find(params[:format])
    @customer.check_out_time = nil
    if @customer.save!
      redirect_to customers_path
    else
      render :index
    end
  end

  def callback_square
    cb = JSON.parse(URI.decode(params[:data])).with_indifferent_access
    if 'ok'.eql?(cb[:status])
      @customer = Customer.find(cb[:state])
      @customer.check_out_time = Time.at(Time.zone.now.to_i / 60 * 60)
      payment = Payment.create(
          transaction_id: cb[:transaction_id],
          client_transaction_id: cb[:client_transaction_id],
          status: cb[:status],
          error_code: cb[:error_code]
      )
      @customer.payment << payment
      if @customer.save!
        redirect_to customers_path
      else
        render :index
      end
    elsif 'error'.eql?(cb[:status])
      @customer.payment.error_code = cb[:error_code]
      flash[:alert] = cb[:error_code]
    end
  end

  def destroy
    if Customer.find_by(id: params[:id]).delete
      redirect_to customers_path
    end
  end

  def split_check
    @customer = Customer.find(params[:id])
    divided = @customer.dup
    divided.people = 1
    @customer.people -= 1

    begin
      divided.save!
      @customer.save!
      redirect_to customers_path
    rescue
      render :index
    end
  end

  def make_payment_request(id, amount)
    req = Customer.make_payment_request(id, amount, current_user.store.currency, Rails.application.secrets.square_client_id)
    redirect_to 'square-commerce-v1://payment/create?data=' + URI.encode(req.to_json)
  end

  private

  def customer_params
    params.require(:customer).permit(%i(
      id
      check_in_time
      check_out_time
      rate_per_min
      store
      charge
      people
      discount_percentage
      kind
      discount
      monthly_member_id
    ))
  end

  def customers_list(d)
    @customers = Customer.customers_list(d, current_user)
                     .page(params[:page])
                     .reverse_order
  end

  def customers_list_today
    @customers = Customer.customers_list_today(current_user)
                     .page(params[:page])
                     .reverse_order
  end
end
