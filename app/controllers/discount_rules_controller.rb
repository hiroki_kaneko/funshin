class DiscountRulesController < ApplicationController
  def index
    @rule = rule
  end

  def new
    respond_to do |format|
      format.json {
        render json: {rule: rule}, status: 200
      }
    end
  end

  private

  def rule
    DiscountRule.where('store_id = ? AND enable = ?', current_user.store.id, true)
  end
end
