class Customer < ApplicationRecord

  has_one :payment, dependent: :destroy, autosave: true
  has_one :monthly_member, dependent: :destroy, autosave: true

  paginates_per 9

  scope :nums_of_checked_out, -> {
    where(check_in_time: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day)
        .where.not(check_out_time: nil).size
  }

  scope :nums_of_checked_in, -> {
    where(check_in_time: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day,
          check_out_time: nil).size
  }

  scope :customers_list, -> (d, current_user) {
    @customers = Customer.where('check_in_time >= ? AND check_in_time < ? AND store = ?',
                                d,
                                d.tomorrow,
                                current_user.store)
  }

  scope :customers_list_today, -> (current_user) {
    @customers = Customer.where('check_in_time >= ? AND check_in_time < ? AND store = ?',
                                Time.zone.now.beginning_of_day,
                                Time.zone.tomorrow.beginning_of_day,
                                current_user.store)
  }

  def self.make_payment_request(id, amount, currency, square_id)
    {
        amount_money: {
            amount: amount,
            currency_code: currency
        },
        callback_url: 'https://morning-shore-21493.herokuapp.com/customers/callback_square',
        client_id: square_id,
        version: '1.3',
        notes: 'Usage Time',
        state: id.to_s,
        options: {
            supported_tender_types: %w(CREDIT_CARD CASH OTHER SQUARE_GIFT_CARD)
        }
    }
  end
end
