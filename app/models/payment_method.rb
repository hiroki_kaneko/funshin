class PaymentMethod < ActiveHash::Base
  self.data = [
      {id: 0, payment_method: 'DEV_USE_ONLY'},
      {id: 1, payment_method: 'CREDIT_CARD'},
      {id: 2, payment_method: 'CASH'},
      {id: 3, payment_method: 'OTHER'},
      {id: 4, payment_method: 'SQUARE_GIFT_CARD'},
      {id: 5, payment_method: 'CARD_ON_FILE'},
  ]
end