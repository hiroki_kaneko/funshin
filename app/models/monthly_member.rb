class MonthlyMember < ApplicationRecord
  has_many :customers
  attr_accessor :charge

  scope :valid_monthly_member, -> (id) {
    where("id = ? AND start_date <= ? AND end_date >= ?", id, Time.zone.now.to_date, Time.zone.now.to_date)
  }

  def build_member

  end
end
