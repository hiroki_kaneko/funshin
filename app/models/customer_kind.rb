class CustomerKind < ActiveHash::Base
  self.data = [
      {id: 0, kind: 'Drop In',        description: '1分毎'},
      {id: 1, kind: 'Monthly Member', description: '月間固定'},
      {id: 2, kind: 'Day Cap',        description: '1日固定'},
      {id: 3, kind: 'Student',        description: '学割'},
  ]
end