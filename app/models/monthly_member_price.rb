class MonthlyMemberPrice < ActiveHash::Base
  self.data = [
      {id: 0, store_id: 2, price: 6000},
      {id: 1, store_id: 1, price: 8000},
      {id: 2, store_id: 3, price: 5000},
  ]
end