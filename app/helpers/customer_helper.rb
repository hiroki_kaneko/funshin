module CustomerHelper

  def currency_symbol(currency)
    case currency
      when 'GBP'
        '&pound;'
      when 'EURO'
        '&euro;'
      when 'RUB'
        '&#x20BD;'
      when 'USD'
        '&#36;'
      when 'JPY'
        '&yen;'
    end
  end

  def curr_usage(check_in_time)
    Time.at((Time.zone.now - check_in_time).to_i).utc.strftime('%k:%M')
  end

  def curr_amount
    # TODO 今の時点での料金を計算する
  end
end
