const dateFormat = 'YYYY-MM-DD';

$(document)
    .on('show.bs.modal', () => {
        let today = new Date();
        let oneMonthLater = dateFns.addMonths(today, 1);
        document.getElementById('startDate').value = dateFns.format(today, dateFormat);
        document.getElementById('endDate').value = dateFns.format(oneMonthLater, dateFormat);
    })
    .on('change', '#startDate', (e) => {
        let oneMonthLater = dateFns.addMonths(new Date(e.currentTarget.value), 1);
        document.getElementById('endDate').value = dateFns.format(oneMonthLater, dateFormat);
    });
