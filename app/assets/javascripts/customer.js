// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// import { isEqual, startOfMinute, differenceInMinutes, format } from 'date-fns';
let checkOutTime;

/**
 * イベント処理
 */
$(document)
    // Rails5+ event when page moved (including first time)
    .on('turbolinks:load', () => {
        let intervalTime = 30000; // it calls per 30sec.

        setInterval((function callAtOnce() {
            printUsageTime();
            return callAtOnce;
        }()), intervalTime);

        $('#checkOut').on('show.bs.modal', (e) => {
            document.querySelector('#checkOut #customer_id').value = e.relatedTarget.dataset.customerId;
            checkOutTime = new Date();
            showModal(e, new Date());
        });
        window.setTimeout("$('.alert-error,.alert-success,.alert-alert,.alert-notice').fadeOut()", 3000);
    })
    .on('input', '#numpeople, #discount_per, #discount_direct, #numpeople', () => {
        reCalculate(checkOutTime);
    })
    .on('change', '#checkouttime', (e) => {
        reCalculate(new Date(e.currentTarget.value));
    })
    .on('change', '#checkintime', () => {
        reCalculate(checkOutTime);
    })
    .on('change', '#customersKind', () => {
        document.querySelector('#checkIn #numberOfPeople').readonly =
            'Monthly Member' === $('#customersKind').find('option:selected').text() ? true : false;
        $('#monthlyMemberId').toggle();
    })
    .submit('#checkOutSubmit', () => {
        if (atLeastOneMin()) {
            toastr["error"]("At least 1 minute needed to Check-Out.");
            return false;
        }
    });

/**
 * チェックイン後、1分以内のチェックアウトを禁止する
 * @returns {boolean | *}
 */
function atLeastOneMin()
{
    let checkIn = document.getElementById('checkintime').value;
    let checkOut = document.getElementById('checkouttime').value;
    return dateFns.isEqual(checkIn, checkOut);
}

/**
 * 利用料金の再計算
 * @param {Object} checkOutTime
 */
function reCalculate(checkOutTime)
{
    let modal       = document.querySelector('#checkOut'); // modal window
    let id          = document.querySelector('#checkOut #customer_id').value;
    let checkInTime = modal.querySelector('#checkintime').value;
    let usageMin    = getUsageMinutes(new Date(checkInTime), checkOutTime);
    let people      = modal.querySelector('#numpeople').value;
    let kindEle     = document.getElementById('kind' + id);
    let kindName    = kindEle.getAttribute('data-kind-name');
    let kindNum     = parseInt(kindEle.getAttribute('data-kind'));
    let percentage  = checkStudent(id, kindName);
    let discount    = modal.querySelector('#discount_direct').value;
    let charge      = calculateCost(id, usageMin, people, discount, percentage);
    let currency    = document.getElementById('kind' + id).getAttribute('data-currency');

    document.querySelector('#discount_per').value = percentage;
    document.getElementById('customer_kind').selectedIndex = kindNum;
    document.getElementById('sub_total').innerHTML = getUsageTimeFormat(usageMin);
    modal.querySelector('#total_cost').innerHTML = getCurrencySymbol(currency) + accounting.formatNumber(charge);
    modal.querySelector('#total_cost_hidden').value = charge;
}

/**
 * チェックアウト モーダルダイアログを表示する
 * @param {Object} e
 * @param {Object} checkOutTime
 */
function showModal(e, checkOutTime)
{
    let target     = $(e.relatedTarget)[0]; // clicked element = 'Check-Out' icon
    let id         = document.querySelector('#checkOut #customer_id').value;
    let people     = target.getAttribute('data-people');
    let usageMin   = getUsageMinutes(new Date(target.getAttribute('data-check-in-time-raw')), checkOutTime);
    let kindEle    = document.getElementById('kind' + id);
    let kindName   = kindEle.getAttribute('data-kind-name');
    let kindNum    = kindEle.getAttribute('data-kind');
    let percentage = checkStudent(id, kindName);
    let discount   = $('#discount_direct').val();
    let currency   = document.getElementById('kind' + id).getAttribute('data-currency');

    let checkInTime = document.querySelector('#checkintime');
    checkInTime.value = target.getAttribute('data-check-in-time');

    let modalOutTime = document.querySelector('#checkouttime');
    let mainOutTime = document.querySelector('#checkOutTime' + id);
    if ("" === mainOutTime.getAttribute('data-checkout').replace(/\r?\n/g, '').trim()) {
        modalOutTime.value = dateFns.format(checkOutTime, 'YYYY-MM-DDTHH:mm');
    }
    else {
        modalOutTime.value = mainOutTime.getAttribute('data-checkout');
    }
    document.querySelector('#discount_per').value = percentage;
    document.getElementById('sub_total').innerHTML = getUsageTimeFormat(usageMin);
    document.getElementById('numpeople').value      = target.getAttribute('data-people');
    document.querySelector('#new_customer [name="customer[id]"]').value = id;
    let charge = calculateCost(id, usageMin, people, discount, percentage);

    if ('Monthly Member' === kindName) {
        charge = document.getElementById('currentCost' + id).getAttribute('data-charge');
    }
    replaceOptions(kindNum);
    document.querySelector('#total_cost').innerHTML = getCurrencySymbol(currency) + accounting.formatNumber(charge);
    document.querySelector('#total_cost_hidden').value = charge;
}

/**
 * リストに現在までの利用料金を表示する
 */
function printUsageTime()
{
    let usageTimeArray = document.getElementsByClassName('usageTime');
    let checkOutTimeArray = document.getElementsByClassName('checkOutTime');

    for (let i = 0; i < usageTimeArray.length; i++) {
        let checkInTime = new Date(usageTimeArray[i].getAttribute('data-checkin'));
        let checkOutTime = checkOutTimeArray[i].getAttribute('data-checkout').replace(/\r?\n/g, '').trim();
        if (!checkOutTime) {
            checkOutTime = new Date();
        }
        let usageMin = getUsageMinutes(checkInTime, checkOutTime);
        usageTimeArray[i].innerHTML = getUsageTimeFormat(usageMin);

        let id = usageTimeArray[i].getAttribute('data-id');
        let currCostObj = document.getElementById('currentCost' + id);
        let currency = currCostObj.getAttribute('data-currency');
        let people = document.getElementById('people' + id).getAttribute('data-people');
        let discount = 0;
        let kind = document.getElementById('kind' + id).getAttribute('data-kind-name');
        let charge = 0;
        if (kind === 'Monthly Member') {
            charge = currCostObj.getAttribute('data-charge');
        }
        else {
            let percentage = checkStudent(id, kind);
            charge = currCostObj.getAttribute('data-charge');
            if (charge === '0.0') {
                charge = calculateCost(id, usageMin, people, discount, percentage);
            }
        }
        currCostObj.innerHTML = accounting.formatMoney(charge, getCurrencySymbol(currency), 0);
    }
}

/**
 * Check-In時刻から現在までを分単位で返す
 * @param {Object} inTime
 * @param {Object} outTime
 * @returns {number | *}
 */
function getUsageMinutes(inTime, outTime)
{
    // 中途半端な秒を00にする
    let round = dateFns.startOfMinute(outTime);
    return dateFns.differenceInMinutes(round, inTime);
}

/**
 * usageTimeをhh:mm形式で返す
 * @param {number} usageTime
 * @returns {string}
 */
function getUsageTimeFormat(usageTime)
{
    return Math.floor(usageTime / 60) + ':' + ('00' + (usageTime % 60)).slice(-2);
}

/**
 * 人数、割引を考慮した支払い額を返す
 * @param {number} id
 * @param {number} usageTime
 * @param {number} people
 * @param {number} discount
 * @param {number} percentage
 * @returns {number}
 */
function calculateCost(id, usageTime, people, discount, percentage)
{
    let currCostObj = document.getElementById('currentCost' + id);
    let ratePerMin = currCostObj.getAttribute('data-rate-per-min');
    let kind = document.getElementById('kind' + id).getAttribute('data-kind-name');
    let dayCapHour = currCostObj.getAttribute('data-day-cap-hour');
    usageTime = checkDayCap(kind, dayCapHour, usageTime);
    let price = (usageTime * ratePerMin) * people;
    let discountPrice = (percentage === 0) ? 0 : price * (percentage / 100);
    return price - discountPrice - discount;
}

/**
 * デイキャップ利用者なら最大利用時間以上は課金されない
 * @param {number} kindType
 * @param {number} dayCapHour
 * @param {number} usageMin
 * @returns {*}
 */
function checkDayCap(kindType, dayCapHour, usageMin)
{
    if ('Day Cap' === kindType) {
        let dayCapMin = dayCapHour * 60;
        if (0 < (usageMin - dayCapMin)) {
            return dayCapMin;
        }
    }
    return usageMin;
}

/**
 * 学生利用者なら割引が適用される
 * @param {number} id
 * @param {string} kindType
 * @returns {*}
 */
function checkStudent(id, kindType)
{
    if ('Student' === kindType) {
        return document.getElementById('currentCost' + id).getAttribute('data-student-discount-percentage');
    }
    return document.getElementById('discount_per').value;
}

/**
 * 実態文字参照で通貨記号を表示する
 * @param {string} currency
 * @returns {string}
 */
function getCurrencySymbol(currency)
{
    switch (currency) {
        case 'GBP':
            return '&pound;';
        case 'EUR':
            return '&euro;';
        case 'RUB':
            return '&#x20BD;';
        case 'USD':
            return '&#36;';
        case 'JPY':
            return '&yen;';
        default:
            console.log('Couldn\'t get currency data');
    }
}

/**
 * モーダル画面表示時に利用者種別をセレクトボックス内で選択する
 * @param {string} value
 */
function replaceOptions(value)
{
    let old = document.querySelector('#new_customer [name="customer[kind]"]').options;
    let options = [];

    for (let i = 0; i < old.length; i++) {
        let $opt = $('<option>', {value: old[i].value, text: old[i].text, selected: parseInt(value) === i});
        options.push($opt);
    }
    $('select#customer_kind option').remove();
    $('select#customer_kind').append(options);
}
