require "administrate/base_dashboard"

class CustomerDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    check_in_time: Field::DateTime,
    check_out_time: Field::DateTime,
    rate_per_min: Field::Number,
    store: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    charge: Field::Number,
    people: Field::Number,
    discount_percentage: Field::Number,
    kind: Field::Number,
    discount: Field::Number,
    payment_method: Field::Number,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :check_in_time,
    :check_out_time,
    :rate_per_min,
    :store,
    :charge,
    :people,
    :discount_percentage,
    :kind,
    :discount,
    :payment_method,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :check_in_time,
    :check_out_time,
    :rate_per_min,
    :store,
    :charge,
    :people,
    :discount_percentage,
    :kind,
    :discount,
    :payment_method,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :id,
    :check_in_time,
    :check_out_time,
    :rate_per_min,
    :store,
    :charge,
    :people,
    :discount_percentage,
    :kind,
    :discount,
    :payment_method,
  ].freeze

  # Overwrite this method to customize how customers are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(customer)
  #   "Customer ##{customer.id}"
  # end
end
