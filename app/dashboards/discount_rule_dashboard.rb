require "administrate/base_dashboard"

class DiscountRuleDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    store: Field::BelongsTo.with_options(searchable: true,
                                         seachable_field: 'name'),
    id: Field::Number,
    name: Field::String,
    reduction: Field::Number.with_options(decimals: 2),
    description: Field::String,
    enable: Field::Boolean,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :store,
    :id,
    :name,
    :reduction,
    :description,
    :enable,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :store,
    :id,
    :name,
    :reduction,
    :description,
    :enable,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :store,
    :name,
    :reduction,
    :description,
    :enable,
  ].freeze

  # Overwrite this method to customize how discount rules are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(discount_rule)
  #   "DiscountRule ##{discount_rule.id}"
  # end
end
