# Funshin

店舗向け顧客滞在時間管理Webアプリケーション。顧客のチェックイン->チェックアウト時間を管理して

Things you may want to cover:

* Ruby version

    2.4.3
* System dependencies

* Configuration

    macOS:
    ```
    gem install nokogiri -- --use-system-libraries --with-iconv-dir="$(brew --prefix libiconv)" --with-xml2-config="$(brew --prefix libxml2)/bin/xml2-config" --with-xslt-config="$(brew --prefix libxslt)/bin/xslt-config"
    ```
* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

チェックイン・アウト時刻の秒は削除して計算する。
```
Check-In:
2018/02/21 15:54:32 => 2018/02/21 15:54:00
Check-Out:
2018/02/21 16:03:59 => 2018/02/21 16:03:00
Total:
9minutes
```

パスワードはsecrets.yml.enc内で管理している。管理するデータはDBMSのID,Passwordと、SquareのApplication ID。
```
production.username
production.password
production.square_client_id
```
この暗号化ファイルはRails 5.1のencrypted secrets機能を利用している。
エンコードされた```secrets.yml.enc```を編集する場合はターミナルから以下のコマンドを入力する。
```
$ EDITOR=vim rails credentials:edit
```
EDITOR定数は好みに合わせて変更可能。

暗号化ファイルのデコードに必要な ```secrets.yml.key``` はセキュリティ上の観点からGit管理しないので、別の手段で管理者からもらう事。


月額メンバーの処理

初回
* チェックイン時
  月額メンバーの登録画面を表示して、期間、金額を入力後、Registボタンを押す。
```monthly_members```テーブルにID、期間のデータが保存される。同時に```customers```テーブルにも、先払いする金額が登録される。
* チェックアウト時

二回目以降
* チェックイン時
チェックイン・ダイアログのKind欄に```Monthly Member```を入力する。

* チェックアウト時
```Monthly Member```の場合は価格は0のまま、価格の変更ができない。```Check-Out```によりチェックアウト時刻のみがDBに書き込まれる。
