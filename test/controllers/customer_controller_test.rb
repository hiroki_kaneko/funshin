require 'test_helper'

class CustomerControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @user = users(:hrk)
    @customer = {
        id: 10,
        check_in_time: Time.zone.now,
        check_out_time: nil,
        rate_per_min: 10,
        store: Store.find_by_name('Tokyo').id,
        charge: 0,
        people: 1,
        discount_percentage: 0,
        kind: CustomerKind.find_by_kind('Drop In').id,
        discount: 0
    }
  end

  test 'callback_square has successfuly result' do
    sign_in @user
    get '/customers'
    assert_response :success
  end

  test 'create' do
    sign_in @user
    post '/customers', params: {customer: @customer}
    assert_redirected_to '/customers'
  end

  test 'search' do
    sign_in @user
    post '/customers/search', params: {search: 1}
    assert_template :index
  end

  test 'search no args' do
    sign_in @user
    post '/customers/search', params: {search: ''}
    assert_redirected_to '/customers'
  end
end
