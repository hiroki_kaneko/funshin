require 'test_helper'

class ChartControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::ControllerHelpers

  test "should get index" do
    get chart_index_url
    assert_response :success
  end

end
