require 'test_helper'

class MonthlyMemberTest < ActiveSupport::TestCase
  test 'valid user' do
    res = MonthlyMember.valid_monthly_member(1)
    assert res
  end
  test 'The user is not exist' do
    res = MonthlyMember.valid_monthly_member(9999999)
    assert_empty res
  end
end
