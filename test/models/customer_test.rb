require 'test_helper'

class CustomerTest < ActiveSupport::TestCase

  def setup
  end

  test 'format of custom json' do
    id = 1
    amount = 100
    currency = 'JPY'
    square_id = 'sq0idp-this-is-test'
    exp_pattern = {
        amount_money: {
            amount: amount,
            currency_code: currency
        },
        callback_url: 'https://morning-shore-21493.herokuapp.com/customers/callback_square',
        client_id: square_id,
        version: '1.3',
        notes: 'Utilization Time',
        state: id.to_s,
        options: {
            supported_tender_types: %w(CREDIT_CARD CASH OTHER SQUARE_GIFT_CARD)
        }
    }
    act_json = Customer.make_payment_request(id, amount, currency, square_id)
    assert_equal exp_pattern, act_json
  end
end
