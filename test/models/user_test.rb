require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "should have default role" do
    user = User.create(id: 101, email: 'bar@example.com', store_id: 2, name: 'bar')
    assert user.has_role? :cashier
  end
end
