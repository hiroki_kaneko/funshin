FactoryBot.define do
  factory :payment do
    transaction_id "MyString"
    client_transaction_id "MyString"
    status "MyString"
    error_code "MyString"
    state "MyString"
  end
end
