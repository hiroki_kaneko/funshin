Rails.application.routes.draw do

  devise_for :users

  root 'customers#index'

  get   'customers/checkout'
  get   'customers/date_change'
  post  'customers/search'
  get   'customers/search_date'
  patch 'customers/cancel'
  get   'customers/split_check'
  get   'customers/make_payment_request'
  get   'customers/callback_square'
  get   'discount_rules/new'
  get   'discount_rules/index'

  resource :customers, only: [:update]
  resources :customers

  resource :monthly_members

  namespace :admin do
    resources :users
    resources :customers
    resources :stores
    resources :discount_rules
  end
end
