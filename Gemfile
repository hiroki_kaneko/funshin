source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

ruby '2.5.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.0'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7' => this ver. didn't work on Windows
gem 'bcrypt-ruby', '~> 3.0.0'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'webpacker', '~> 3.3.0'

# ログイン機構
gem 'devise', '~> 4.4.1'
# マスタメンテ画面
gem 'administrate', '~> 0.10.0'
# マスタテーブル
gem 'active_hash', '~> 2.0.0'
# フロントエンド
gem 'bootstrap', '~> 4.1', '>= 4.1.1'
gem 'jquery-rails', '~> 4.3.1'
# pagination
gem 'kaminari', '~> 1.1.1'
# icons
gem 'font-awesome-sass', '~> 5.0.6'
# 警告
gem 'toastr_rails', '~> 2.1', '>= 2.1.3'
# 権限
gem 'rolify', '~> 5.2.0'

group :test do
  gem 'sqlite3', '~> 1.3.13'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver', '~> 3.11.0'
  gem 'brakeman', require: false
  gem 'rails-controller-testing', '~> 1.0.2'
end

group :development, :test do
  gem 'faker', '~> 1.8.7'
  gem 'pry-rails', '~> 0.3.6'
  gem 'pry-byebug', '~> 3.6.0'
  gem 'rubocop', require: false
end

group :production do
  gem 'rails_12factor', '~> 0.0.3'
end

group :development, :production do
  gem 'pg', '~> 1.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
