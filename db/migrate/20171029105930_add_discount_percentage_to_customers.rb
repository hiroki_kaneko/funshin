class AddDiscountPercentageToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :discount_percentage, :integer
  end
end
