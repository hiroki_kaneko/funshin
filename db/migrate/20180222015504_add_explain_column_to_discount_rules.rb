class AddExplainColumnToDiscountRules < ActiveRecord::Migration[5.1]
  def change
    add_column :discount_rules, :message, :string
  end
end
