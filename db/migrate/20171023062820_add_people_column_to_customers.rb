class AddPeopleColumnToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :people, :integer
  end
end
