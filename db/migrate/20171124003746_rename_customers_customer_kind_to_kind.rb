class RenameCustomersCustomerKindToKind < ActiveRecord::Migration[5.1]
  def change
    rename_column :customers, :customer_kind, :kind
  end
end
