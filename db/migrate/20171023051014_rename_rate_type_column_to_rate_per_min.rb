class RenameRateTypeColumnToRatePerMin < ActiveRecord::Migration[5.1]
  def change
    rename_column :customers, :rate_type, :rate_per_min
  end
end
