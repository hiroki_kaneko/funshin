class ChangeDatatypeRatePerMinToDoubleOfStores < ActiveRecord::Migration[5.1]
  def change
    change_column :stores, :rate_per_min, :float
  end
end
