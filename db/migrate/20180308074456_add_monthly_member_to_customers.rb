class AddMonthlyMemberToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_reference :customers, :monthly_member, foreign_key: true
  end
end
