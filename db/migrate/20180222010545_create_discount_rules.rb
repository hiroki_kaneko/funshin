class CreateDiscountRules < ActiveRecord::Migration[5.1]
  def change
    create_table :discount_rules do |t|
      t.references :store
      t.string :name
      t.float :reduction

      t.timestamps
    end
  end
end
