class AddColumnStudentDiscountToStores < ActiveRecord::Migration[5.1]
  def change
    add_column :stores, :student_discount_percentage, :integer
  end
end
