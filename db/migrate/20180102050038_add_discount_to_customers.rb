class AddDiscountToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :discount, :integer, default: 0
  end
end
