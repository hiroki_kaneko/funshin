class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments, {id: :bigserial} do |t|
      t.string :transaction_id
      t.string :client_transaction_id
      t.string :status
      t.string :error_code
      t.string :state

      t.timestamps
    end
  end
end
