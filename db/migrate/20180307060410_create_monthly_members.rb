class CreateMonthlyMembers < ActiveRecord::Migration[5.1]
  def change
    create_table :monthly_members do |t|
      t.string :uuid, null: false
      t.date :start_date, null: false
      t.date :end_date, null: false
      t.references :customer, null: false
      t.timestamps
    end
  end
end
