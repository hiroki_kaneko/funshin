class AddRateToStore < ActiveRecord::Migration[5.1]
  def change
    add_column :stores, :rate_per_min, :integer
  end
end
