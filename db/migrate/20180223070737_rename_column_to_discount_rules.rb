class RenameColumnToDiscountRules < ActiveRecord::Migration[5.1]
  def change
    rename_column :discount_rules, :message, :description
  end
end
