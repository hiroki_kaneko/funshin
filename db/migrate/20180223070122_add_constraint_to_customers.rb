class AddConstraintToCustomers < ActiveRecord::Migration[5.1]
  def change
    change_column_default :customers, :people, 1
    change_column_default :customers, :kind, 1

    change_column_null :customers, :rate_per_min, false
    change_column_null :customers, :store, false
    change_column_null :customers, :people, false
    change_column_null :customers, :kind, false
  end
end
