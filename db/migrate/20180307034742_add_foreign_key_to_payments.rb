class AddForeignKeyToPayments < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :payments, :customers, on_delete: :cascade
  end
end
