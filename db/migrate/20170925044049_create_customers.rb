class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers, {id: :bigserial} do |t|
      t.string :name
      t.datetime :check_in_time
      t.datetime :check_out_time
      t.integer :rate_type
      t.integer :store

      t.timestamps
    end
  end
end
