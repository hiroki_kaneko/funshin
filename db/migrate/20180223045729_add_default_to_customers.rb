class AddDefaultToCustomers < ActiveRecord::Migration[5.1]
  def change
    change_column_default :customers, :discount_percentage, 0
    change_column_default :customers, :discount, 0
    change_column_default :customers, :charge, 0
    change_column_default :customers, :payment_method, 2

    change_column_null :customers, :discount_percentage, false
    change_column_null :customers, :discount, false
    change_column_null :customers, :charge, false
    change_column_null :customers, :payment_method, false
  end
end
