class RemoveCustomerIdToMonthlyMembers < ActiveRecord::Migration[5.1]
  def change
    remove_column :monthly_members, :customer_id, :integer
  end
end
