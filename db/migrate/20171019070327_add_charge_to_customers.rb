class AddChargeToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :charge, :integer, after: :store
  end
end
