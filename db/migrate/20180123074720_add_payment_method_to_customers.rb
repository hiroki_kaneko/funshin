class AddPaymentMethodToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :payment_method, :integer, default: 2
  end
end
