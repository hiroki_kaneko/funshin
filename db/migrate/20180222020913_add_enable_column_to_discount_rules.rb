class AddEnableColumnToDiscountRules < ActiveRecord::Migration[5.1]
  def change
    add_column :discount_rules, :enable, :boolean
  end
end
