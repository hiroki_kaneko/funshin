class AddLocationColumnToStores < ActiveRecord::Migration[5.1]
  def change
    add_column :stores, :location, :string
  end
end
