class AddCustomerKindToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :customer_kind, :integer
  end
end
