# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Store.create(id: 1, name: 'London', currency: 'GBP', rate_per_min: 8.2, day_cap_hour: 4, student_discount_percentage: 25)
Store.create(id: 2, name: 'Tokyo', currency: 'JPY', rate_per_min: 10, day_cap_hour: 3, student_discount_percentage: 20)
Store.create(id: 3, name: 'Oita', currency: 'JPY', rate_per_min: 10, day_cap_hour: 3, student_discount_percentage: 25)

User.create(id: 1, email: 'hrk@example.com', password: '123456', created_at: Time.zone.now, updated_at: Time.zone.now, store_id: Store.find_by_name('Tokyo').id, name: 'Hiroki Kaneko')
User.create(id: 2, email: 'foo@example.com', password: '123456', created_at: Time.zone.now, updated_at: Time.zone.now, store_id: Store.find_by_name('London').id, name: 'Foo')
User.create(id: 3, email: 'bar@example.com', password: '123456', created_at: Time.zone.now, updated_at: Time.zone.now, store_id: Store.find_by_name('Tokyo').id, name: 'Bar')

[:admin, :manager, :cashier].each do |role|
  Role.find_or_create_by(name: role)
end

User.find(1).add_role :admin
User.find(2).add_role :manager
User.find(3).add_role :cashier

# 10.times do |n|
#   c_in = Faker::Time.between(Time.zone.now - 30.day, Time.zone.now, :day)
#   c_out = Faker::Time.between(c_in, c_in + Random.rand(1..100))
#   Customer.create(id: n,
#                   check_in_time: c_in,
#                   check_out_time: c_out,
#                   rate_per_min: 10,
#                   store: 2,
#                   created_at: c_in,
#                   updated_at: c_out,
#                   charge: Random.rand(10..1000),
#                   discount_percentage: 0,
#                   kind: 1,
#                   people: Random.rand(1..5),
#                   discount: 0,
#                   payment_method: 2)
# end
ActiveRecord::Base.connection.execute("ALTER SEQUENCE customers_id_seq RESTART WITH 1")
ActiveRecord::Base.connection.execute("ALTER SEQUENCE monthly_members_id_seq RESTART WITH 1")
